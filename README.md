# Interface du REAC pour auto évaluation (2nde partie)

## Instruction d'installation

1- Clonez le repo dans le dossier que vous voulez mettre le projet avec la commande suivante dans un terminal:

    git clone https://gitlab.com/nraynaud31/interface-du-reac-pour-auto-evaluation.git

2- Ouvrez un terminal dans le dossier de l'interface REAC, et exécutez la commande suivante dans un terminal:

    npm start


Le lien du site est le suivant: https://rocky-springs-15465.herokuapp.com/
