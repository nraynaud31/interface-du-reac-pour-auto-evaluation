import { Request, Response } from "express-serve-static-core";
import Criteres from "../models/Criteres";
import User from "../models/User";
import User_has_crit from "../models/User_has_crit";

export default class CriteresResultController {


    static get(req: Request, res: Response): void {
        // On récupère tous les critères qui ont étés cochés dans la pivot
        const database = req.app.locals.db

        // @ts-ignore
        const user_has_criterescol = User_has_crit.getAllfromUserId(req.session.user);

        res.render('pages/result', {
            title: "ON A REUSSI !!!",
            user_has_crit: user_has_criterescol,
        });
    }

    static save(req: Request, res: Response): void {
        const database = req.app.locals.db
        const status_session = req.session;
        const id = req.body.compId;
        console.log("result = ", req.body)
        const criteres_in_DB = Criteres.getAllFromCompetenceId(id);
        // @ts-ignore
        const pivot = User_has_crit.getAllFromUser(req.session.user);
        let tableau: any[] = []
        pivot.forEach((element: any) => {
            tableau.push(element.criteres_id_criteres)
        });
        console.log(tableau);

        for (let i of criteres_in_DB) {
            // ID du critère
            const critere_ID = i.id_criteres;
            const checkBoxId = req.body["checkbox" + critere_ID];
            // On récupère les critères en question
            // @ts-ignore
            const user_id = req.session.user;
            const data = User_has_crit.getAllfromUserIdAndCriteresId(user_id, checkBoxId);
            // @ts-ignore
            console.log(req.session.user);
            // On vérifie que les critères dans la base sont undefined pour éviter les doublons
            console.log("ID critere test:", critere_ID)
            console.log("Checkboxid: " + checkBoxId)
            if (checkBoxId !== undefined) {
                if (!tableau.includes(+checkBoxId)) {
                    // @ts-ignore
                    const insert = User_has_crit.addCritere(user_id, checkBoxId);
                    console.log(insert);
                } else {

                }
            } else if (tableau.includes(critere_ID)) {
                // @ts-ignore
                User_has_crit.deleteCritere(req.session.user, critere_ID);
            }
        }
        res.redirect('/')
        // res.render('pages/result', {
        //     title: "Critères Validés",
        //     status_session: status_session,
        // });
    }

    static update(req: Request, res: Response): void {
        const database = req.app.locals.db
        const status_session = req.session;
        const id = req.params.user_has_criterescol;
        for (let i in req.body) {
            const id_body = req.body[i];
            // @ts-ignore
            const data = User_has_crit.getAllfromUserIdAndCriteresId(req.session.user, id_body);
            if (data !== undefined) {
                // On met à jour l'information qui est présente dans la DB
                // @ts-ignore
                User_has_crit.update(req.session.user, id_body);
            }
        }

        res.render('pages/result', {
            title: "Critères update",
            status_session: status_session,
        });
    }

    static delete(req: Request, res: Response): void {
        const database = req.app.locals.db
        const status_session = req.session;
        const id = req.params.user_has_criterescol;
        for (let i in req.body) {
            // ID des critères concernés
            const critereID = req.body[i];

            // @ts-ignore
            const criteres_in_pivotdb = User_has_crit.getAllfromUserIdAndCriteresIdPivot(req.session.user, critereID);
            for (let j of criteres_in_pivotdb) {
                if (critereID) {
                    if (criteres_in_pivotdb !== undefined) {
                        // On supprime la donnée
                        // @ts-ignore
                        User_has_crit.deleteFromUserIdAndCritereId(req.session.user, critereID)
                    }
                }
            }
            res.render('pages/result', {
                title: "Critères supprimés",
                status_session: status_session,
            });
        }
 
        res.render('pages/result', {
            title: 'Critère(s) supprimé(s)',
            status_session: status_session,
        });
    }

    /*static test(name: String): void {
        var elems = document.querySelectorAll('[name=fruit[]')
        for (var i = 0; i < elems.length; i++) {
            var isChecked = elems[i].checked;
            console.log(isChecked);
        }
    }*/
}