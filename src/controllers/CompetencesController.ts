import { Request, Response } from "express-serve-static-core";
import Competences from "../models/Competences";
import Criteres from "../models/Criteres";
import User_has_crit from "../models/User_has_crit";

export default class CompetencesController {

    static competences(req: Request, res: Response): void {
        const database = req.app.locals.db
        const status_session = req.session;
        const id = req.params.id_competences;
        const competence = Competences.getAllFromId(+id);
        const criteres = Criteres.getAllFromCompetenceId(+id);
        //@ts-ignore
        const criteres_id = User_has_crit.getAllfromUserId(Number(req.session.user));
        let tableau: any[] = []
        criteres_id.forEach((e: any) => {
            tableau.push(e.criteres_id_criteres);
            
        }); console.log(tableau)

        criteres.forEach((element: any) => {
            if (tableau.includes(element.id_criteres)) {
                element.checked = "checked"
                
            }
            else { element.checked = "" }
        });


        res.render('pages/competences', {
            title: competence.title,
            description: competence.description,
            criteres_content: criteres,
            status_session: status_session,
        });
    }
}