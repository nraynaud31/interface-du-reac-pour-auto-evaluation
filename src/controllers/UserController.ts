import { Request, Response } from "express-serve-static-core";
import User from "../models/User";
import HomeController from "./HomeController";
import bcrypt from 'bcrypt';



export default class CriteresResultController {


    static login(req: Request, res: Response): void {
        const db = req.app.locals.db;
        let password = req.body.password
        let email = req.body.email;
        const user = User.getByEmail(email);
        console.log("user: ",user);
        if(user === undefined){            
            res.redirect('/login')
            return
        }
        let comparaison_password = bcrypt.compareSync(password, user.password);
        if (user.email === email && comparaison_password) {
            console.log(user);
            //res.redirect('/');
            //@ts-ignore
            req.session.email = email
            // @ts-ignore
            req.session.user = user.id_user;

            //@ts-ignore
            req.session.firstname = user.firstname;
            // @ts-ignore
            console.log(req.session.user)
            res.redirect('/')
        } else {
            console.log("L'ID n'est pas bon!");
            res.render('pages/login', {
                message: 'Identifiants incorrectes',
                title: 'Se connecter',
            })
        }
    }

    static register(req: Request, res: Response): void {
        const db = req.app.locals.db;
        console.log(req.body);
        let email = req.body.email;
        let password = req.body.password;
        let hash = bcrypt.hashSync(password, 1);
        const user_register = User.getAll();
        let tableau: any[] = []
        // Permettre de vérifier si l'utilisateur est déjà inscrit avec cet adresse email
        const verif = User.getByEmail(email);
        user_register.forEach((element: any) => {
            tableau.push(element.email)
        });
        if (!tableau.includes(email)) {
            User.createUser(req.body.firstname, req.body.lastname, email, hash);
            res.redirect('/login')
        } else {
            res.render('pages/register', {
                title: "S'enregistrer",
                error: 'Erreur: un utilisateur possédant cet adresse email existe déjà dans la base de données !',
                verif: verif,
            })
            console.log("vérif bdd: ",verif)
            // HomeController.showRegister(req, res);
        }
    }

    static logout(req: Request, res: Response): void {
        if(req.session){
            req.session.destroy(err => {
                if(!err){
                    // res.redirect('/')
                    res.redirect('/')
                } else {

                }
            })
        } else {
            res.end();
        }
    }
}