import {database} from '../server';
import { TCompetences, TCriteres } from '../types';

export default class Criteres {

    static getAllFromCompetenceId(id: number) : TCriteres[] {
        return database.prepare('SELECT * FROM criteres WHERE competences_id_competences=?').all(id);
    }
}