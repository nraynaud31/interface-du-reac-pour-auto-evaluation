import { RunResult } from 'better-sqlite3';
import {database} from '../server';
import {TUser_has_Criteres } from '../types';
import User from './User';

export default class User_has_crit {

    static getAllfromUserId(id: any): TUser_has_Criteres[] {
        return database.prepare("SELECT * FROM user_has_criteres WHERE user_id_user=?").all(id);
    }

    static getAllfromUserIdeach(id: any): TUser_has_Criteres[]{
        return database.prepare("SELECT * FROM user_has_criteres WHERE user_id_user=?").all(id);
    }

    static getAllFromUser(id: any): TUser_has_Criteres[] {
        return database.prepare("SELECT * FROM user_has_criteres WHERE user_id_user=?").all(id);
    }

    static getAllfromUserIdAndCriteresId(user_id: number, critere_id: number): void {
        return database.prepare('SELECT * FROM user_has_criteres WHERE user_id_user = ? AND criteres_id_criteres = ?').get(user_id, critere_id);
    }

    static getAllfromUserIdAndCriteresIdPivot(user_id: number, critere_id: number): TUser_has_Criteres[] {
        return database.prepare('SELECT * FROM user_has_criteres WHERE user_id_user = ? AND criteres_id_criteres = ?').get(user_id, critere_id);
    }

    static addCritere(user_id: number, critere_id: number): RunResult {
        return database.prepare("INSERT INTO user_has_criteres ('user_id_user','criteres_id_criteres') VALUES (?, ?)").run(user_id, critere_id);
    }

    static deleteCritere(user_id: number, critere_id: number): RunResult {
        return database.prepare("DELETE FROM user_has_criteres WHERE user_id_user=? AND criteres_id_criteres=?").run(user_id, critere_id);
    }

    static update(user_id: number, critere_id: number): RunResult {
        return database.prepare("UPDATE user_has_criteres SET user_id_user = ? AND criteres_id_criteres= ?").run(user_id, critere_id);
    }

    static deleteFromUserIdAndCritereId(user_id: number, critere_id: number) : RunResult {
        return database.prepare("DELETE FROM user_has_criteres WHERE user_id_user = ? AND criteres_id_criteres= ?").run(user_id, critere_id);

    }
}