import { RunResult } from 'better-sqlite3';
import {database} from '../server';
import { TUser } from '../types';

export default class User {

    static createUser(firstname: string, lastname: string, email: string, hashed_password: string): RunResult {
        return database.prepare('INSERT INTO user ("firstname", "lastname", "email", "password") VALUES (?, ?, ?, ?)').run(firstname, lastname, email, hashed_password);
    }

    static getByEmail(email: string): TUser {
        return database.prepare("SELECT * from user WHERE email=?").get(email);
    }

    static getAll(): any {
        return database.prepare("SELECT * FROM user").all();
    }
}