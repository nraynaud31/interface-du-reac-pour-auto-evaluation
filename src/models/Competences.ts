import {database} from '../server';
import { TCompetences } from '../types';

export default class Competences {

    static getAllFromId(id: number): TCompetences {
        return database.prepare("SELECT * FROM competences WHERE id_competences=?").get(id);
    }

    static getAll(): any {
        return database.prepare("SELECT * FROM competences").all();
    }
}