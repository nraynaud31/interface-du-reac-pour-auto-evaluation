// Import dependencies
import path from "path";
import fs from 'fs';
import express from 'express';
import bodyParser from 'body-parser';
import Database from 'better-sqlite3';
import session from 'express-session';
const { config, engine } = require('express-edge');
import cookieParser from "cookie-parser";


// Import router
import router from './router';

// Initialize the express engine
const app: express.Application = express();

// Take a port 3000 for running server.
const port = process.env.PORT || 3000;

// On définit le fichier qui va contenir les données
export const database = new Database(path.join(__dirname, './database/data.db'), { verbose: console.log });
// On définit une propriété pour la db
app.locals.db = database
/**
 * creation de la base de données, et insertion des données
 * A ne lancer qu'une seule fois
 */

//try {
// const migration = fs.readFileSync(path.join(__dirname, '/database/sql-scripts/reac-structure.sql'), 'utf8');
// database.exec(migration);
// const migrationData = fs.readFileSync(path.join(__dirname, '/database/sql-scripts/reac-data.sql'), 'utf8');
// database.exec(migrationData);
//}
//catch (error) {

//}

// define the templating engine
app.use(engine);
// define the Views folder
app.set("views", path.join(__dirname, "./views"));

// public folder
app.use(
    express.static(path.join(__dirname, "public"), { maxAge: 31557600000 })
);

// Cookies
const oneDay = 1000*60*60*24;
app.use(session({ 
    secret: 'thisismyfuckingsecretkey',
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: oneDay },
}));
app.use(cookieParser());



// to read request form body
app.use(bodyParser.urlencoded({ extended: false }));

// Redirections
export const redirectUser = (req: Request, res: any, next:any) => {
    //@ts-ignore
    if(!req.session.email){
        res.redirect('/login')
    }
    else{
        next();
    }
}


// Routes
router(app);

// Server setup
app.listen(port, () => {
    console.log(`TypeScript with Express http://localhost:${port}/`);
    //console.log(database.prepare("SELECT id_competences FROM competences").all());
});