import { Application } from "express";
import CompetencesController from "./controllers/CompetencesController";
import CriteresResultController from "./controllers/CriteresResultController";
import HomeController from "./controllers/HomeController";
import UserController from "./controllers/UserController";
import { redirectUser } from "./server";
import { Request, ParamsDictionary, Response } from "express-serve-static-core";
import { ParsedQs } from "qs";
import { body, validationResult } from "express-validator";


export default function route(app: Application) {
    /** Static pages **/
    app.get('/', (req, res) => {
        HomeController.index(req, res);
    });

    app.get('/about', (req, res) => {
        HomeController.about(req, res);
    });

    app.get('/competences/:id_competences', redirectUser, (req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>, number>, next: any) => {
        req.params.id
        CompetencesController.competences(req, res);
    });


    app.post('/comp/result', (req, res) => {
        CriteresResultController.save(req, res);
    });

    app.post('/comp/result', (req, res) => {
        CriteresResultController.delete(req, res);
    });
    app.post('comp/result', (req, res) => {
        CriteresResultController.update(req, res);
    });

    app.get('/comp/result', (req, res) => {
        CriteresResultController.get(req, res);
    });

    app.get('/login', (req, res) => {
        HomeController.showLogin(req, res);
    })

    app.post('/login', (req, res) => {
        UserController.login(req, res);
    })

    app.get('/register', (req, res) => {
        HomeController.showRegister(req, res);
    })
    
    app.post('/register', (req, res) => {
        UserController.register(req, res);
    })

    // app.post('/register',
    //     body('firstname').isLength({ min: 3 }),
    //     body('lastname').isLength({ min: 3 }),
    //     body('email').isLength({ min: 5 }),
    //     body('password').isStrongPassword({
    //         minLength: 3,
    //         minLowercase: 1,
    //         minUppercase: 1,
    //         minNumbers:1
    //     }).withMessage("Le mot de passe doit être plus grand que 3 caractères, posséder au moins une majuscule, une minuscule, et un nombre"),
    //     (req, res) => {
    //         const errors = validationResult(req)
    //         if (!errors.isEmpty()) {
    //             res.render('pages/register')
    //         }
    //         // @ts-ignore
    //         UserController.register(req, res);
    //    })

    app.get('/logout', (req, res) => {
        UserController.logout(req, res);
    });

}

