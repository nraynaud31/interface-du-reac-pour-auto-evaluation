-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Xavplon
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2021-12-10 16:08
-- Created:       2021-12-06 13:59
PRAGMA foreign_keys = OFF;

-- Schema: mydb
-- Toujours commenter le ATTACH
--ATTACH "mydb.sdb" AS "mydb";
--Commence le debut du fichier 
BEGIN;
CREATE TABLE "competences"(
  "id_competences" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "title" VARCHAR(100) NOT NULL,
  "modified_at" TIMESTAMP,
  "deleted_at" TIMESTAMP,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "description" LONGTEXT NOT NULL
);
CREATE TABLE "user"(
  "id_user" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "firstname" VARCHAR(255) NOT NULL,
  "lastname" VARCHAR(255) NOT NULL,
  "email" VARCHAR(255) NOT NULL,
  "password" VARCHAR(255) NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "modified_at" TIMESTAMP,
  "deleted_at" TIMESTAMP
);
CREATE TABLE "criteres"(
  "id_criteres" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "content" LONGTEXT NOT NULL,
  "modified_at" TIMESTAMP,
  "deleted_at" TIMESTAMP,
  "competences_id_competences" INTEGER NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT "fk_criteres_competences1"
    FOREIGN KEY("competences_id_competences")
    REFERENCES "competences"("id_competences")
);
CREATE INDEX "criteres.fk_criteres_competences1_idx" ON "criteres" ("competences_id_competences");
CREATE TABLE "user_has_criteres"(
  "user_id_user" INTEGER NOT NULL,
  "criteres_id_criteres" INTEGER NOT NULL,
  "user_has_criterescol" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  CONSTRAINT "fk_user_has_criteres_user"
    FOREIGN KEY("user_id_user")
    REFERENCES "user"("id_user"),
  CONSTRAINT "fk_user_has_criteres_criteres1"
    FOREIGN KEY("criteres_id_criteres")
    REFERENCES "criteres"("id_criteres")
);
CREATE INDEX "user_has_criteres.fk_user_has_criteres_criteres1_idx" ON "user_has_criteres" ("criteres_id_criteres");
CREATE INDEX "user_has_criteres.fk_user_has_criteres_user_idx" ON "user_has_criteres" ("user_id_user");
COMMIT;
