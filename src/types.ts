export type TCompetences = {
    id_competences: number;
    title: string;
    modified_at: string;
    deleted_at: string;
    created_at: string;
    description: string;
}

export type TCriteres = {
    id_criteres: number;
    content: string;
    modified_at: string;
    deleted_at: string;
    competences_id_competences: number;
    created_at: string;
}

export type TUser = {
    id_user: number;
    firstname: string;
    lastname: string;
    email: string;
    password: string;
    created_at: string;
    modified_at: string;
    deleted_at: string;
}

export type TUser_has_Criteres = {
    user_id_user: number;
    criteres_id_criteres: number;
    user_has_criterescol: number;
}